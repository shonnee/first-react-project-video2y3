import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faMagnifyingGlass, faCartShopping } from '@fortawesome/free-solid-svg-icons';

const Header = styled.header`

    width: 100%;
    height: 30px;
    margin-bottom: 30px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    :nth-child(1){
        justify-content: center;
    }

`;

const IconDiv = styled.div`

    box-sizing: border-box;
    width: fit-content;
    height: auto;
    color: #aeaeae;
    :nth-child(1){
        padding: 0px 5px;
        height: auto;
    }

`;

const NavBar = styled.nav`

    box-sizing: border-box;
    width: 70%;
    height: auto;
    color: #aeaeae;
    padding: 5px 0px;
    margin: 0px 10px;
    background-color: #f2f2f2;
    border-radius: 8px;
    display: flex;

`;

const Input = styled.input`

    box-sizing: border-box;
    width: 80%;
    height: 18px;
    font-family: Verdana, Geneva, Tahoma, sans-serif;
    font-size: 12px;
    border: none;
    color: #aeaeae;
    background-color: transparent;
    outline: none;

`;

const MiniCircle = styled.span`

    box-sizing: border-box;
    width: 10px;
    height: 10px;
    border-radius: 50%;
    background-color: #ff7744;
    position: relative;
    right: 2%;
    bottom: 35%;

`

export const HeaderApp = () => {

    return (

        <Header>

            <IconDiv>

                <FontAwesomeIcon icon={faBars}/>

            </IconDiv>

            <NavBar>

            <IconDiv>

                <FontAwesomeIcon icon={faMagnifyingGlass}/>

            </IconDiv>

            <Input 
            
                placeholder='Type something...'
            
            />

            </NavBar>

            <IconDiv>

                <FontAwesomeIcon icon={faCartShopping}/>

            </IconDiv>

            <MiniCircle/>

        </Header>

    )

};
