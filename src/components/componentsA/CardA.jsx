import React from 'react';
import styled from 'styled-components';

/* IMG's */
import waveSpan from '../../img/wave-span.png';

const Container = styled.div`

    box-sizing: border-box;
    width: 100%;
    height: 40%;
    border-radius: 12px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    background-color: #ffefea;
    color: #fea47b;
    z-index: 0;


`;

const BottomSpan = styled.img`

    box-sizing: border-box;
    width: 100%;
    height: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    border-bottom-left-radius: 12px;
    border-bottom-right-radius: 12px;

`;

const TextDiv = styled.div`

    box-sizing: border-box;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    text-transform: uppercase;
    font-style: italic;
    font-weight: bold;
    font-size: 10px;
    text-align: center;
    width: 70%;
    margin-top: 15px;
    display: flex;
    justify-content: center;
    align-items: center;

`;

const ButtonDiv = styled.button`

    box-sizing: border-box;
    width: 80px;
    height: 25px;
    color: whitesmoke;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    text-transform: uppercase;
    font-style: italic;
    font-weight: bold;
    font-size: 10px;
    border-radius: 20px;
    border: none;
    background-color: #fea37d;
    display: flex;
    align-items: center;
    justify-content: center;
    position: absolute;
    bottom: 66.2%;
    z-index: 2;

`;

export const CardAppA = () => {
    
    return (

        <>
        
            <Container>
            
                <TextDiv>

                    use code: pigi100 for rs.100 off on your first order!

                </TextDiv>

                <ButtonDiv>

                    grab now

                </ButtonDiv>

                <BottomSpan src={waveSpan}/>
            
            </Container>
        
        </>

    
    )

};
