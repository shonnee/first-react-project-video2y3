import React from 'react';
import styled from 'styled-components';

const Header = styled.header`

    width: 100%;
    height: auto;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;

`;

const MiniSpan = styled.span`

    box-sizing: border-box;
    width: 100%;
    height: auto;
    text-align: center;
    margin: 0px 0px 25px 0px;
    font-size: 12px;
    text-transform: uppercase;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    font-weight: bolder;

`;

const NavList = styled.nav`

    box-sizing: border-box;
    width: 100%;
    height: auto;
    margin: 0px;
    display: flex;
    flex-direction: row;
    text-align: center;
    padding-bottom: 20px;

`;

const LiList = styled.li`

    box-sizing: border-box;
    width: 100%;
    height: auto;
    margin: 0px;
    list-style: none;
    font-size: 12px;
    opacity: 0.5;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    :nth-child(3){
        font-weight: bold;
        opacity: 1;
    }
    ::after{
        content: "";
        position: absolute;
        top: 248px;
        left: 380px;
        right: 0;
        margin: 0 auto;
        width: 20px;
        height: 2px;
        background-color: #ffffff;
    }
    text-transform: uppercase;

`;

export const HeaderApp = () => {

    return (

        <Header>

            <MiniSpan>

                filter

            </MiniSpan>
            <NavList>

                <LiList>

                    by color

                </LiList>
                <LiList>

                    by size

                </LiList>
                <LiList>

                    by brand

                </LiList>
                <LiList>

                    by price

                </LiList>

            </NavList>

        </Header>

    )

};
