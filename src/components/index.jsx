import React from "react";
import styled from "styled-components";

/* Components A */
import { HeaderApp } from "./componentsA/Header";
import { CardAppA } from "./componentsA/CardA";
import { CardAppB } from "./componentsA/CardB";
import { SectionApp } from "./componentsA/Section";

/* Components B */
import { BackgroundApp } from "./componentsB/Background";


const ContainerA = styled.section`

    box-sizing: border-box;
    width: 100%;
    max-width: 300px;
    height: 650px;
    margin: 10px;
    padding: 45px 15px;
    border-radius: 30px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    background-color: white;

`;

const ContainerB = styled.section`

    box-sizing: border-box;
    width: 100%;
    max-width: 300px;
    height: 650px;
    margin: 10px;
    padding-top: 30px;
    border-radius: 30px;
    color: white;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
    background-color: #2a2a2a;

`;


export const IndexApp = () => {
    
    return (

        <>

            <ContainerA>

                <HeaderApp/>

                <CardAppA/>

                <SectionApp/>

                <CardAppB/>

            </ContainerA>

            <ContainerB>

                <BackgroundApp/>

            </ContainerB>

        </>

    )

};



