import React from 'react';
import styled from 'styled-components';

/* IMG's */
import productRight from '../../img/productRight.png';
import productLeft from '../../img/productLeft.png';

const ProductSection = styled.section`

    box-sizing: border-box;
    width: 100%;
    height: 70%;
    padding: 10px 10px 20px 10px;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-between;
    align-items: center;

`;

const ProductList = styled.ul`

    box-sizing: border-box;
    width: 100%;
    height: 20%;
    margin: 0px;
    padding: 0px;
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    align-items: center;

`;

const ListOption = styled.li`

    box-sizing: border-box;
    width: 100%;
    height: 100%;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    font-size: 12px;
    color: #d2d4d4;
    font-size: 11px;
    text-transform: uppercase;
    font-weight: bold;
    display: flex;
    justify-content: center;
    align-items: center;
    :first-child{
        display: flex;
        position: relative;
        color: #806ecf;
    }::after{
        content: "";
        position: absolute;
        bottom: 5px;
        left: 0;
        right: 0;
        margin: 0 auto;
        width: 30%;
        height: 2px;
        background-color: #806ecf;
    }
    :nth-child(2){
        width: 200%;
    }

`;

const ProductContainer = styled.div`

    box-sizing: border-box;
    width: 48%;
    height: 62%;
    display: flex;
    flex-direction: column;
    filter: opacity(0.55);

`;

const Img = styled.img`

    box-sizing: border-box;
    width: 100%;
    height: 90%;
    background-color: #fcf8ef;
    border-radius: 10px;

`;

const Description = styled.p`

    box-sizing: border-box;
    width: 100%;
    height: 20px;
    text-transform: uppercase;
    font-weight: bold;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    font-size: 12px;
    color: #acacac;
    margin: 5px 0px 0px;

`;

export const SectionApp = () => {

    return (

        <ProductSection>

            <ProductList>

                <ListOption>

                    proteins

                </ListOption>
                <ListOption>

                    pre/post workout

                </ListOption>
                <ListOption>

                    others

                </ListOption>

            </ProductList>
        
            <ProductContainer>

                <Img src={productLeft}/>
                <Description>
                    whey proteins
                </Description>

            </ProductContainer>
            <ProductContainer>

                <Img src={productRight}/>
                <Description>
                    whey isolates
                </Description>

            </ProductContainer>
        
        </ProductSection>

    )

};
