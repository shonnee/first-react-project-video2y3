import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

/* IMGs */
import greenTick from '../../img/green-tick.png';
import redCross from '../../img/red-cross.png';
import redMiniTick from '../../img/red-mini-tick.png';
import violetUndo from '../../img/violet-undo.png';

const Container = styled.div`

    box-sizing: border-box;
    width: 100%;
    height: auto;
    display: flex;
    justify-content: center;
    padding: 10px 15px 0px;

`;

const Text = styled.div`

    box-sizing: border-box;
    width: 100%;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    font-size: 13px;
    height: auto;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    text-transform: uppercase;

`;

const IconContainer = styled.div`

    box-sizing: border-box;
    width: 25px;
    height: 25px;
    margin: 0px;
    border: 1px solid white;
    border-radius: 50px;
    object-fit: cover;

`;

const Icon = styled.img`

    box-sizing: border-box;
    width: 100%;
    height: 100%;
    margin: 0px;
    padding: 2px;
    border-radius: 50px;
    object-fit: cover;
    visibility: ${props => props.visible ? 'visible' : 'hidden'};
    background-color: ${props => props.visible ? 'white' : 'transparente'};

`;

const PairContainer = styled.span`

    box-sizing: border-box;
    width: 120px;
    height: auto;
    position: absolute;
    bottom: 20%;
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    align-items: center;

`;

const PairButtons = styled.img`

    box-sizing: border-box;
    width: 35%;
    height: 80%;
    margin: 0px 5px;
    padding: 5px;
    border: 1px transparent white;
    background-color: white;
    border-radius: 50px;

`;

export const OptionsApp = ({txt, state}) => {

    const [visible, setVisible] = useState(false);

    useEffect(() => {

        setVisible(!state);

    }, [state])

    const handleChange = () => {

        setVisible(!visible);

    }

    return (

        <>
        
            <Container>

                <Text>

                    {`${txt}`}


                    <IconContainer

                        visible={visible}
                        onClick={handleChange}

                    >

                        <Icon

                            src={redMiniTick}
                            visible={visible}
                            
                            />

                    </IconContainer>

                </Text>
                <PairContainer>

                    <PairButtons src={redCross}/>
                    <PairButtons src={violetUndo}/>
                    <PairButtons src={greenTick}/>

                </PairContainer>

            </Container>
        
        </>

    )

};