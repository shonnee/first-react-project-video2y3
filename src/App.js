import React from "react";
import styled from "styled-components";
import { IndexApp } from "./components";

const BigContainer = styled.div`

    box-sizing: border-box;
    height: 900px;
    margin: 10px;
    background-color: #c4bebb;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: center;
    align-items: center;

`;

function App() {

    return (

        <BigContainer>

            <IndexApp/>

        </BigContainer>

    );

}

export default App;
