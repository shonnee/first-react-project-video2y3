import React from 'react';
import styled from 'styled-components';
import { OptionsApp } from './Options';

const Container = styled.div`

    box-sizing: border-box;
    width: 100%;
    height: 55%;
    border-radius: 20px;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    background-color: #21d0d1;
    color: white;

`;

export const CardAppB = () => {

    const options = [

        { txt: 'price low to high', state: false },
        { txt: 'price high to low', state: false },
        { txt: 'popularity', state: true }

    ]

    return (

        <>
        
            <Container>

                {options.map((v, i) => 
                
                    <OptionsApp key={i} {...v} />
                
                )}

            </Container>
        
        </>

    )

};
