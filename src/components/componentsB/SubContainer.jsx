import React from 'react';
import styled from 'styled-components';
import { OptionsApp } from '../componentsB/Options';

const SubContainer = styled.section`

    box-sizing: border-box;
    width: 115%;
    height: 440px;
    padding: 0px 10px 0px 10px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    border-top: 1px solid rgba(255, 255, 255, 0.4);
    border-bottom: 1px solid rgba(255, 255, 255, 0.4);

`;



export const SubContainerApp = () => {

    let options = [

        { txt: '1up nutrition' },
        { txt: 'asitis' },
        { txt: 'avvatar' },
        { txt: 'big muscles' },
        { txt: 'bpi sports' },
        { txt: 'bsn' },
        { txt: 'cellucor' },
        { txt: 'domin8r' },
        { txt: 'dymatize' },
        { txt: 'dynamik' },
        { txt: 'esn' },
        { txt: 'evolution nutrition' },

    ]
    
    return (

        <>

                <SubContainer>

                    {options.map((v, i) => <OptionsApp key={i} {...v}/>)}

                </SubContainer>

        </>

    )

};