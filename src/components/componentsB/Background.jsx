import React from 'react';
import styled from 'styled-components';

/* Imports */
import { HeaderApp } from './Header';
import { SubContainerApp } from './SubContainer';

const Container = styled.div`

    box-sizing: border-box;
    width: 100%;
    height: 100%;
    border-radius: 20px;
    padding: 20px;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    background-color: #ff7744;

`;

export const BackgroundApp = () => {
    
    return (

            <Container>

                <HeaderApp/>

                <SubContainerApp/>
            
            </Container>

    )

};
